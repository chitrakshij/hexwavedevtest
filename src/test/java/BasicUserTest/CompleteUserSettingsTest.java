package BasicUserTest;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeTest;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterSuite;

public class CompleteUserSettingsTest 
{
	WebDriver driver;
	Properties prop;
	@Test(priority=1)
	  public void succesfullyLoginAdvanceUser() throws InterruptedException
	  {
		WebElement email = driver.findElement(By.id("email"));
		email.sendKeys(prop.getProperty("MailId"));
		WebElement passcode1 = driver.findElement(By.xpath("//input[@formcontrolname='passcode1']"));
		passcode1.sendKeys(prop.getProperty("passcode1"));
		WebElement passcode2 = driver.findElement(By.xpath("//input[@formcontrolname='passcode2']"));
		passcode2.sendKeys(prop.getProperty("passcode2"));
		WebElement passcode3 = driver.findElement(By.xpath("//input[@formcontrolname='passcode3']"));
		passcode3.sendKeys(prop.getProperty("passcode3"));
		WebElement passcode4 = driver.findElement(By.xpath("//input[@formcontrolname='passcode4']"));
		passcode4.sendKeys(prop.getProperty("passcode4"));

		driver.findElement(By.className("globalButton")).submit();
		System.out.println("------------------------------------------------------");
		System.out.println("Email ID :- " + prop.getProperty("MailId"));
		Thread.sleep(5000); 
	  }
	 @Test(priority=2,dependsOnMethods = "succesfullyLoginAdvanceUser")
	  public void clickOnUserSettings() throws InterruptedException
	  {
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
		  
		  WebElement nav_btn = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[1]/app-top-navbar-content/div[1]/div[1]/div/div/div[1]/button"));
		  executor.executeScript("arguments[0].click()", nav_btn);
		  
		  WebElement user = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav/div/app-side-menu-content/app-condense/div/a[13]/span"));
		  executor.executeScript("arguments[0].click()", user);
	  }
	  
	  @Test(priority=3,dependsOnMethods = "clickOnUserSettings")
	  public void changeBetaTestMode() throws InterruptedException
	  {
		  String flag;
		  
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
	      Thread.sleep(5000);
	      
	      WebElement mode = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[2]/div/mat-slide-toggle/label/div/input"));
	      flag = mode.getAttribute("aria-checked");
	      System.out.println("Current Mode:-"+mode.getAttribute("aria-checked"));
	      
	      if(flag.equals("false"))
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[2]/div/mat-slide-toggle/label/div"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into True:-ON");
	    	  
	      }
	      else
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[2]/div/mat-slide-toggle/label/div/input"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into False:-OFF");
	      }
	      WebElement save=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[5]/div/input"));
	      save.click();
	  }
	  @Test(priority=4,dependsOnMethods = "clickOnUserSettings")
	  public void changePrimaryGaurdMode() throws InterruptedException
	  {
		  String flag;
		  
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
	      Thread.sleep(5000);
	      
	      WebElement mode = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[3]/div/mat-slide-toggle/label/div/input"));
	      flag = mode.getAttribute("aria-checked");
	      System.out.println("Current Mode:-"+mode.getAttribute("aria-checked"));
	      
	      if(flag.equals("false"))
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[3]/div/mat-slide-toggle/label/div/input"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into True:-ON");
	    	  
	      }
	      else
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[3]/div/mat-slide-toggle/label/div/input"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into False:-OFF");
	      }
	      WebElement save=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[5]/div/input"));
	      save.click();
	  }
	 
	  @Test(priority=5 ,dependsOnMethods = "clickOnUserSettings")
	  public void changeSendThreatsToHexwaveMode() throws InterruptedException
	  {
		  String flag;
		  
		  Thread.sleep(1000);
		  JavascriptExecutor executor = (JavascriptExecutor)driver;
	      Thread.sleep(5000);
	      
	      WebElement mode = driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[4]/div/mat-slide-toggle/label/div/input"));
	      flag = mode.getAttribute("aria-checked");
	      System.out.println("Current Mode:-"+mode.getAttribute("aria-checked"));
	      
	      if(flag.equals("false"))
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[4]/div/mat-slide-toggle/label/div/input"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into True:-ON");
	    	  
	      }
	      else
	      {
	    	  WebElement changebetamode=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[4]/div[4]/div/mat-slide-toggle/label/div/input"));
	    	  executor.executeScript("arguments[0].click()", changebetamode);
	    	  System.out.println("Change Into False:-OFF");
	      }
	      WebElement save=driver.findElement(By.xpath("/html/body/app-root/app-admin-page/div[2]/div/mat-sidenav-container/mat-sidenav-content/div/app-settings-page/div/div/div/div/form/div[5]/div/input"));
	      save.click();
	  }
	  
	  @Test(priority=6)
	  public void checkUserSettingInDB() throws Exception
	  {
		  	Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection
		    ("jdbc:mysql://104.198.155.142:3306/hexwave_device_setting?createDatabaseIfNotExist=true", "hexwave", "h3xW@veDevAugust13");
		
			Statement stmt = con.createStatement();
			
			ResultSet rs = stmt.executeQuery("SELECT * FROM   hexwave.threat_setting where id=1");
			while (rs.next()) 
			{
			System.out.println("------------------------------------------------------");
			System.out.println("Status From DataBase");
			System.out.println("Beta Test Mode Status is:-"+rs.getBoolean("beta_test_mode")+"   ");
			System.out.println("Primary Guard Mode Status is:-"+rs.getBoolean("primary_guard_mode")+"   ");
			System.out.println("Send Threats To HexWave Status is:-"+rs.getBoolean("send_threats_to_hexwave")+"   ");
			System.out.println("------------------------------------------------------");
			}
	  }
  @BeforeTest
  public void beforeTest() throws IOException 
  {
	  	WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("headless");
		driver = new ChromeDriver(options);
		//driver = new ChromeDriver();

		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(400, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		prop = new Properties();
		FileInputStream ip=new FileInputStream("./config2.properties");
		prop.load(ip); 
		System.out.println("Url :-" + prop.getProperty("URL"));
		System.out.println("loading.........");
		driver.get(prop.getProperty("URL"));
  }

  @AfterSuite
  public void afterSuite() 
  {
	  driver.close();
	  System.out.println("All Test Cases Pass");

  }

}
